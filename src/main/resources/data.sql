INSERT INTO usuario(id, login, senha) VALUES (null, 'teste_user', '$2a$10$OZIv7QI9sLYWpyD1lvFv6uxZ2Lo33UHsCPcmShP6dSU5nxszVDlEW'); -- senha '123456'

INSERT INTO usuario(id, login, senha) VALUES (null, 'teste_admin', '$2a$10$OZIv7QI9sLYWpyD1lvFv6uxZ2Lo33UHsCPcmShP6dSU5nxszVDlEW'); -- senha '123456'


INSERT INTO perfil(id, descricao) VALUES (null, 'user');

INSERT INTO perfil(id, descricao) VALUES (null, 'admin');



INSERT INTO usuario_perfil(id, id_usuario, id_perfil) VALUES (null, 1, 1);

INSERT INTO usuario_perfil(id, id_usuario, id_perfil) VALUES (null, 2, 2);