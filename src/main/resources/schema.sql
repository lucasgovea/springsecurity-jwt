CREATE TABLE usuario (
  id INTEGER PRIMARY KEY auto_increment,
  login VARCHAR(255) NOT NULL,
  senha VARCHAR(255) NOT NULL
);


CREATE TABLE perfil (
  id  INTEGER PRIMARY KEY auto_increment,
  descricao VARCHAR(255) NOT NULL
);


CREATE TABLE usuario_perfil (
  id  INTEGER PRIMARY KEY auto_increment,
  id_usuario INTEGER NOT NULL,
  id_perfil INTEGER NOT NULL,
  foreign key (id_usuario) references usuario(id),
  foreign key (id_perfil) references perfil(id)
);