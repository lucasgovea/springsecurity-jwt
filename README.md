# Descrição

Projeto de back end basico de exemplo criado utilizando as seguintes tecnologia:  
- Spring boot  
- Spring Security  
- JWT  
- H2 database  

### Autenticação
## Admin

``` curl --request POST \
  --url http://localhost:8080/api/auth/signin \
  --header 'Content-Type: application/json' \
  --data '{
		"username": "teste_admin", 
		"password": "123456" 
}' ```

## User

``` curl --request POST \
  --url http://localhost:8080/api/auth/signin \
  --header 'Content-Type: application/json'
  --data '{
		"username": "teste_admin", 
		"password": "123456" 
}' ```

### Listar Usuários

``` curl --request GET \
  --url http://localhost:8080/api/usuarios \
  --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0ZV91c2VyIiwiaWF0IjoxNjQxODE5MDYxLCJNeVNlY3JldFdvcmRUb0pXVFNlY3VyaXR5Ijp7InBhc3N3b3JkIjpudWxsLCJ1c2VybmFtZSI6InRlc3RlX3VzZXIiLCJhdXRob3JpdGllcyI6W3siYXV0aG9yaXR5IjoidXNlciJ9XSwiYWNjb3VudE5vbkV4cGlyZWQiOnRydWUsImFjY291bnROb25Mb2NrZWQiOnRydWUsImNyZWRlbnRpYWxzTm9uRXhwaXJlZCI6dHJ1ZSwiZW5hYmxlZCI6dHJ1ZX0sImV4cCI6MTY0MTkwNTQ2MX0.9q7QC-6HJT3dUjyxT89D-8Rff0GCzRTFwbgkyEUhI0p0LLKHKpUisFmNEXQ5h-oqn8ib5Ca8r6GBkKJpAnd8jw' \
  --header 'Content-Type: application/json'
 ```
 
### Buscar Usuário

``` curl --request GET \
  --url http://localhost:8080/api/usuarios/2 \
  --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0ZV91c2VyIiwiaWF0IjoxNjQxODE5MDYxLCJNeVNlY3JldFdvcmRUb0pXVFNlY3VyaXR5Ijp7InBhc3N3b3JkIjpudWxsLCJ1c2VybmFtZSI6InRlc3RlX3VzZXIiLCJhdXRob3JpdGllcyI6W3siYXV0aG9yaXR5IjoidXNlciJ9XSwiYWNjb3VudE5vbkV4cGlyZWQiOnRydWUsImFjY291bnROb25Mb2NrZWQiOnRydWUsImNyZWRlbnRpYWxzTm9uRXhwaXJlZCI6dHJ1ZSwiZW5hYmxlZCI6dHJ1ZX0sImV4cCI6MTY0MTkwNTQ2MX0.9q7QC-6HJT3dUjyxT89D-8Rff0GCzRTFwbgkyEUhI0p0LLKHKpUisFmNEXQ5h-oqn8ib5Ca8r6GBkKJpAnd8jw' \
  --header 'Content-Type: application/json' \
 ```

